option explicit
'
' Kill all processes by the name of strProcessName running under the current user
'
Dim strComputer,strProcessName, strOwner, strUserName
Dim objWMI, objNetwork, colProcesses, dummy, objProcess
Dim oShell
Dim UserName
 
Set oShell = Wscript.CreateObject("Wscript.Shell")
'Set strComputer = WScript.Arguments

strComputer = oShell.ExpandEnvironmentStrings( "%COMPUTERNAME%" )
'WScript.Echo "Running Against Remote Computer Named: " & strComputer

UserName = oShell.ExpandEnvironmentStrings("%USERNAME%")
'WScript.Echo "Looking for Microsoft Word for the following loged in user: " &  UserName

'strProcessName = "WINWORD.EXE"
strProcessName = "jp2launcher.exe"
 
set objWMI = GetObject("winmgmts:\\" & strComputer & "\root\cimv2")
set objNetwork = CreateObject("WScript.Network")
set colProcesses = objWMI.ExecQuery("Select * from Win32_Process Where Name = '"& strProcessName & "'")


strUserName = objNetwork.UserName
for each objProcess in colProcesses
    dummy = objProcess.GetOwner(strOwner)
    if (strOwner = strUserName) then
    objProcess.Terminate()
	end if
next
 
set objWMI = nothing
set objNetwork = nothing
set objProcess = nothing