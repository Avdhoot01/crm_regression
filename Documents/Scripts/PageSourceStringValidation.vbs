Dim strPhrase, filepath

Set Arg = WScript.Arguments

Dim folderName
folderName = "..\.."
Dim fso
Set fso = CreateObject("Scripting.FileSystemObject")
Dim fullpath
fullpath = fso.GetAbsolutePathName(folderName)

filepath=fullpath+ "\DOCUMENTS\PAGE SOURCE.TXT"

strPhrase= Arg(0)

strContent = ReadTextFile(filepath, 0)
arrContent = Split(strContent, strPhrase)

Wscript.Echo UBound(arrContent)
'MsgBox "Number of times phrase occurs: " & UBound(arrContent)

Function ReadTextFile(strPath, lngFormat)
    ' lngFormat -2 - System default, -1 - Unicode, 0 - ASCII
    With CreateObject("Scripting.FileSystemObject").OpenTextFile(strPath, 1, False, lngFormat)
        ReadTextFile = ""
        If Not .AtEndOfStream Then ReadTextFile = .ReadAll
        .Close
    End With
End Function