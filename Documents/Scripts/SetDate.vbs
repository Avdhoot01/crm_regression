'GINGER_Description Pass new date in format mm/dd/yyyy hh:mm:ss AM -> returns date in format : yyy-mm-dd 00:00
'GINGER_$ NewDate

newDate = WScript.Arguments(0)

Dim epoch_time
if cint(Day(newDate))<10 then
	epoch_time=Year(newDate) & "-" & "0" & Month(newDate) & "-" & "0" & Day(newDate) & " 00:00"
Elseif cint(Day(Now()))>10 then
	epoch_time=Year(newDate) & "-" & "0" & Month(newDate) & "-" & Day(newDate) & " 00:00"
End if
Wscript.Echo "~~~GINGER_RC_START~~~"
Wscript.Echo "epoch_time="&epoch_time
Wscript.Echo "~~~GINGER_RC_END~~~"