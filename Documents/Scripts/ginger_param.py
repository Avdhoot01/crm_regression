import pandas as pd
"""
read ginger parameter excel,
read parameters from command line 
acept n no of agroumetnns from commanfline , corresponding to n  different excel parameters

"""

import sys

def parse_arguments():
    """
    function to parce command line argument
    return dictionary with argument value pair
    """
    print ('Number of arguments:', len(sys.argv), 'arguments.')
    print ('Argument List:', str(sys.argv))
    return_dict={}
    for arg in sys.argv:
        if "=" in arg:
            argument=arg.split("=")
            return_dict[argument[0]]=argument[1]
        else:
            print("ignoring {} as its nt valid format, use key=value format to provide argument".format(arg))

    return return_dict

def update_parameter(params=None):
    """
    update excel with params dict
    """
    #create copy sanityCalndar
    df=pd.read_excel('C:\GingerIST\Documents\Calendar\IST_Regression\SanityCalender.xlsx')
    print(df.head())
    print(params['tcname'])
    df=df[df['TestCase']==params['tcname']]
    #params=params.popitem('tcname')
    #print(params)
    print(df)
    for param in params.keys():
        if param == 'tcname':
            pass
        elif param not in df.columns:
            print("Aborting !! invalid parameter {}, not found in original excel".format( param))
            return -1 

        else:
               
            df[param]=params[param]
            
    print(df)      
    #OPtion 2 update path   in Gigner Script
    #option1 directly edit SanityCalender , 
    print(df.to_excel('C:\GingerIST\Documents\Calendar\IST_Regression\test.xlsx',sheet_name='Sanity INPUT', index=False))
    return 0

if __name__ == "__main__":
    params=parse_arguments()
    if 'tcname' not in params:
        print("Aborting !! missing TCName")
        #exit(1)
    elif len(params) <1:
        print("Aborting !! no parmater")
        #exit(1)
    else:
        update_parameter(params)
        print("sucess!! updated parameters")
        #exit(0)    